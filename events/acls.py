import requests
import json
from .keys import PEXEL_API_KEY, OPEN_WEATHER_API_KEY


def get_location_photo(city, state):
    headers = {"Authorization": PEXEL_API_KEY}
    url = f"https://api.pexels.com/v1/search/?query={city}+{state}"
    resp = requests.get(url, headers=headers)
    content = resp.json()["photos"][0]["url"]  # or however you want to pull data out of nested json
    return content


def get_location_weather(city, state):
    #Get lat and lon of city
    params = {
        "q": f"{city},{state},US",
        "appid": OPEN_WEATHER_API_KEY
    }

    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = response.json()  # or: <json.loads(response.content)>
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

   #Get the weather
    params = {
        "lat": latitude,
        "lon": longitude,
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    content = response.json()
    try:
        temperature = content["main"]["temp"]
        description = content["weather"][0]["description"]
        return {"temp": temperature, "description": description}
    except (KeyError, IndexError):
        return None
