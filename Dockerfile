FROM python:3
# Select the base image that is best for our application


# Install any operating system junk




#ENV PYTHONUNBUFFERED 1
#creates environmental variable that allows us to see python logs

WORKDIR /app
# Set the working directory to copy stuff to

COPY accounts accounts
COPY attendees attendees
COPY common common
COPY conference_go conference_go
COPY events events
COPY presentations presentations
COPY requirements.txt requirements.txt
COPY manage.py manage.py
#puts everything from current directory into working directory /app
# Copy all the code from the local directory into the image
#you CAN specify exactly what you do or dont want to copy: ex: API keys

RUN pip install -r requirements.txt
# run a terminal command in our container
# Install any language dependencies

CMD gunicorn --bind 0.0.0.0:8000 conference_go.wsgi
#runs application server inside Docker container
